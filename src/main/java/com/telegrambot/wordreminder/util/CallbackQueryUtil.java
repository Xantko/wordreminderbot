package com.telegrambot.wordreminder.util;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import com.telegrambot.wordreminder.handler.enumeration.InlineButtonData;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CallbackQueryUtil {

    public static CallbackQueryHandlerName getCallbackQueryHandlerName(String queryData) {
        return CallbackQueryHandlerName.valueOf(queryData.split("\\|")[0]);
    }

    public static String buildCallbackQueryData(CallbackQueryHandlerName name, InlineButtonData data) {
        return String.format("%s|%s", name.name(), data.name());
    }

    public static InlineButtonData getCallbackQueryButtonData(String queryData) {
        return InlineButtonData.valueOf(queryData.split("\\|")[1]);
    }
}
