package com.telegrambot.wordreminder.util.constant;

public class MessageKeyConstant {
    private MessageKeyConstant() {
    }

    public static final String MESSAGE_ADD_NEW_WORD = "reply.word.addNewWordCmd";
    public static final String MESSAGE_ADD_MEANING = "reply.word.addMeaning";
    public static final String MESSAGE_ADD_PHRASE = "reply.word.addPhrase";
    public static final String MESSAGE_EMPTY_MEANING = "reply.word.emptyMeaning";
    public static final String MESSAGE_EMPTY_PHRASE = "reply.word.emptyPhrase";
    public static final String MESSAGE_ADD_EXTRA_PHRASE = "reply.word.askToAddExtraPhrase";
    public static final String MESSAGE_FINISH_PROCESS = "reply.word.finishProcess";


    public static final String INLINE_BUTTON_ADD_EXTRA_PHRASE = "reply.word.button.addExtraPhrase";
    public static final String INLINE_BUTTON_STOP_ADDING_PHRASE = "reply.word.button.stopAddingPhrase";


    public static final String MENU_BUTTON_ADD_NEW_WORD = "reply.button.mainMenu.addNewWord";
    public static final String MENU_BUTTON_WORD_MANAGING = "reply.button.mainMenu.wordManaging";
    public static final String MENU_BUTTON_SETTINGS = "reply.button.mainMenu.settings";
    public static final String MENU_BUTTON_HELP = "reply.button.mainMenu.help";
}
