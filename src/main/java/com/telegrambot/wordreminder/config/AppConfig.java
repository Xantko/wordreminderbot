package com.telegrambot.wordreminder.config;

import com.telegrambot.wordreminder.handler.TelegramHandlerFacade;
import com.telegrambot.wordreminder.handler.WordReminderTelegramBot;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.ApiContext;

@Configuration
@RequiredArgsConstructor
public class AppConfig {

    private final BotConfiguration configuration;

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public WordReminderTelegramBot getWebhookHandler(TelegramHandlerFacade telegramHandlerFacade) {
        DefaultBotOptions options = ApiContext
                .getInstance(DefaultBotOptions.class);
        WordReminderTelegramBot wordReminderTelegramBot = new WordReminderTelegramBot(options, telegramHandlerFacade);
        wordReminderTelegramBot.setBotUsername(configuration.getUsername());
        wordReminderTelegramBot.setBotToken(configuration.getToken());
        wordReminderTelegramBot.setBotPath(configuration.getUrl());

        return wordReminderTelegramBot;
    }
}
