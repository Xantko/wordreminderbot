package com.telegrambot.wordreminder.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Data
@Configuration
@ConfigurationProperties(prefix = "telegram-bot")
public class BotConfiguration {

    private String username;

    private String token;

    private String url;
}
