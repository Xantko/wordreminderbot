package com.telegrambot.wordreminder.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum LangCode {
    RU("ru-RU"),
    EN("en-US");

    private final String name;
}
