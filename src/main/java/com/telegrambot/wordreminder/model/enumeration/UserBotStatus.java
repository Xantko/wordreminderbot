package com.telegrambot.wordreminder.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UserBotStatus {
    MAIN_MENU,
    HELP,
    LANGUAGE_SETTINGS,
    REMINDER_SETTINGS,
    SET_WEEKLY_WORDS_NUMBER,
    SET_TIMEZONE,
    VERIFY_TIMEZONE,
    SET_FIRST_STAGE_TIME,
    SET_SECOND_STAGE_TIME,
    SET_THIRD_STAGE_TIME,
    VERIFY_TIME,
    ADD_NEW_WORD_CMD,
    ADD_NEW_WORD,
    ADD_MEANING,
    ADD_PHRASE_CMD,
    ADD_PHRASE,
    ASK_TO_ADD_EXTRA_PHRASE,
    STOPPED_BOT,
    SETTINGS_MENU,
    WORD_MANAGING,
    CHOOSE_LANG_NEW_USER;
}
