package com.telegrambot.wordreminder.model.enumeration;

import lombok.Getter;

@Getter
public enum WordStatus {

    FIRST,
    SECOND,
    THIRD
}
