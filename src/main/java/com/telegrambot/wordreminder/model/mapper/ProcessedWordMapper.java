package com.telegrambot.wordreminder.model.mapper;

import com.telegrambot.wordreminder.model.dto.ProcessedWordDTO;
import com.telegrambot.wordreminder.model.jpa.ProcessedWord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProcessedWordMapper extends ModelMapper<ProcessedWord, ProcessedWordDTO> {
}
