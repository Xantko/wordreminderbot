package com.telegrambot.wordreminder.model.mapper;

import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.jpa.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends ModelMapper<User, UserDTO> {

}
