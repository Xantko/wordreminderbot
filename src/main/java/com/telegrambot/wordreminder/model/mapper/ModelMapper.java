package com.telegrambot.wordreminder.model.mapper;

public interface ModelMapper<E, D> {

    E toEntity(D dto);

    D toDTO(E entity);
}
