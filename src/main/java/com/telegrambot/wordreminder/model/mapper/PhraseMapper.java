package com.telegrambot.wordreminder.model.mapper;

import com.telegrambot.wordreminder.model.dto.PhraseDTO;
import com.telegrambot.wordreminder.model.jpa.Phrase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PhraseMapper extends ModelMapper<Phrase, PhraseDTO> {
}
