package com.telegrambot.wordreminder.model.mapper;

import com.telegrambot.wordreminder.model.dto.WordDTO;
import com.telegrambot.wordreminder.model.jpa.Word;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WordMapper extends ModelMapper<Word, WordDTO> {
}
