package com.telegrambot.wordreminder.model.dto;

import com.telegrambot.wordreminder.model.enumeration.WordStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WordDTO {

    private Long id;

    private String word;

    private String meaning;

    private WordStatus status;

    private List<PhraseDTO> phrases;
}
