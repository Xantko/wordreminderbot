package com.telegrambot.wordreminder.model.dto;

import com.telegrambot.wordreminder.model.enumeration.LangCode;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;

    private Long userNumber;

    private String name;

    private String surname;

    private Long chatNumber;

    private LangCode langCode;

    private UserBotStatus status;

}
