package com.telegrambot.wordreminder.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessedWordDTO {

    private Long id;

    private UserDTO user;

    private WordDTO word;

}
