package com.telegrambot.wordreminder.model.jpa;

import com.telegrambot.wordreminder.model.enumeration.WordStatus;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "word", schema = "public")
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String word;

    private String meaning;

    @Enumerated(EnumType.STRING)
    private WordStatus status;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private User user;

    @OneToMany(mappedBy = "word", orphanRemoval = true,
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Phrase> phrases;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "word", fetch = FetchType.LAZY)
    private ProcessedWord processedWord;
}
