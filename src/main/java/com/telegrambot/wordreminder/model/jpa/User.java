package com.telegrambot.wordreminder.model.jpa;

import com.telegrambot.wordreminder.model.enumeration.LangCode;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Entity
@Table(name = "user", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long userNumber;

    @NotNull
    private String name;

    private String surname;

    @NotNull
    private Long chatNumber;

    @Enumerated(EnumType.STRING)
    private LangCode langCode;

    private LocalDateTime createdDate;

    @Enumerated(EnumType.STRING)
    private UserBotStatus status;

    @OneToMany(mappedBy = "user", orphanRemoval = true,
            cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Word> words;

    @OneToOne(mappedBy = "user")
    private WeeklyWords weeklyWords;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
    private ProcessedWord processedWord;
}
