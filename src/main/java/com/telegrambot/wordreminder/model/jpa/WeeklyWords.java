package com.telegrambot.wordreminder.model.jpa;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "weekly_words", schema = "public")
public class WeeklyWords {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String wordIds;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

//    public List<Long> getWordIds() {
//        return List.of(wordIds.split(",")).stream().map(Long::valueOf).collect(Collectors.toList());
//    }
//
//    public void setWordIds(List<Long> wordIds) {
//        this.wordIds = wordIds.stream().map(Object::toString).collect(Collectors.joining(","));
//    }
}
