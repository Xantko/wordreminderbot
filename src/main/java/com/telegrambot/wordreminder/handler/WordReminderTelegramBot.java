package com.telegrambot.wordreminder.handler;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.Update;

@Data
@EqualsAndHashCode(callSuper = false)
public class WordReminderTelegramBot extends TelegramWebhookBot {

    String botPath;
    String botUsername;
    String botToken;


    private TelegramHandlerFacade telegramHandlerFacade;

    public WordReminderTelegramBot(DefaultBotOptions options, TelegramHandlerFacade telegramHandlerFacade) {
        super(options);
        this.telegramHandlerFacade = telegramHandlerFacade;
    }

    @Override
    public BotApiMethod onWebhookUpdateReceived(Update update) {
        return telegramHandlerFacade.handleUpdate(update);
    }
}
