package com.telegrambot.wordreminder.handler.callbackquery;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

@Component
@RequiredArgsConstructor
public class CallbackQueryHandlerSettings implements CallbackQueryHandler {

    @Override
    public SendMessage handleCallbackQuery(CallbackQuery callbackQuery) {
        return null;
    }

    @Override
    public CallbackQueryHandlerName getHandlerQueryType() {
        return CallbackQueryHandlerName.SETTINGS;
    }
}
