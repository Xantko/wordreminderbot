package com.telegrambot.wordreminder.handler.callbackquery;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import com.telegrambot.wordreminder.handler.enumeration.InlineButtonData;
import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import com.telegrambot.wordreminder.service.ReplyMessageService;
import com.telegrambot.wordreminder.service.UserService;
import com.telegrambot.wordreminder.util.CallbackQueryUtil;
import com.telegrambot.wordreminder.util.constant.MessageKeyConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

@Component
@RequiredArgsConstructor
public class CallbackQueryHandlerWord implements CallbackQueryHandler {

    private final UserService userService;
    private final ReplyMessageService messageService;

    @Override
    public SendMessage handleCallbackQuery(CallbackQuery callbackQuery) {
        InlineButtonData data = CallbackQueryUtil.getCallbackQueryButtonData(callbackQuery.getData());
        UserDTO user = userService.getUserByUserNumber(callbackQuery.getFrom().getId().longValue());
        switch (data) {
            case ADD_PHRASE:
                return addAnotherPhrase(callbackQuery, user);
            case STOP_ADDING_PHRASE:
                return finishAddNewWordProcess(callbackQuery, user);
        }
        return null;
    }

    @Override
    public CallbackQueryHandlerName getHandlerQueryType() {
        return CallbackQueryHandlerName.WORD;
    }

    private SendMessage addAnotherPhrase(CallbackQuery callbackQuery, UserDTO user) {
        userService.updateUserStatus(callbackQuery.getFrom().getId().longValue(), UserBotStatus.ADD_PHRASE);
        return messageService.getReplyMessage(callbackQuery.getMessage().getChatId(), MessageKeyConstant.MESSAGE_ADD_PHRASE, user.getLangCode().getName());
    }

    private SendMessage finishAddNewWordProcess(CallbackQuery callbackQuery, UserDTO user) {
        userService.updateUserStatus(callbackQuery.getFrom().getId().longValue(), UserBotStatus.MAIN_MENU);
        return messageService.getReplyMessage(callbackQuery.getMessage().getChatId(), MessageKeyConstant.MESSAGE_FINISH_PROCESS, user.getLangCode().getName());
    }
}
