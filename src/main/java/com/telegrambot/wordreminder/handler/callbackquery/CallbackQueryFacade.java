package com.telegrambot.wordreminder.handler.callbackquery;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import com.telegrambot.wordreminder.util.CallbackQueryUtil;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

@Component
public class CallbackQueryFacade {

    public SendMessage processCallbackQuery(CallbackQuery usersQuery) {
        CallbackQueryHandlerName usersQueryType = CallbackQueryUtil.getCallbackQueryHandlerName(usersQuery.getData());

        return null;
    }
}
