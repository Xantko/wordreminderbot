package com.telegrambot.wordreminder.handler.callbackquery;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

public interface CallbackQueryHandler {

    SendMessage handleCallbackQuery(CallbackQuery callbackQuery);

    CallbackQueryHandlerName getHandlerQueryType();
}
