package com.telegrambot.wordreminder.handler.enumeration;

public enum CallbackQueryHandlerName {
    WORD,
    SETTINGS
}
