package com.telegrambot.wordreminder.handler.enumeration;

public enum HandlerName {
    WORD,
    SETTINGS,
    HELP,
    MAIN,
    EXCEPTION
}
