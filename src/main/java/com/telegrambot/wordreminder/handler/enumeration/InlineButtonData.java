package com.telegrambot.wordreminder.handler.enumeration;


public enum InlineButtonData {

    ADD_PHRASE,
    STOP_ADDING_PHRASE
}
