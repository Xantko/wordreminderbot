package com.telegrambot.wordreminder.handler.message;

import com.telegrambot.wordreminder.handler.enumeration.HandlerName;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public class HelpHandler implements MessageHandler {

    @Override
    public SendMessage handle(Message message) {
        return null;
    }

    @Override
    public HandlerName getHandlerName() {
        return HandlerName.HELP;
    }
}
