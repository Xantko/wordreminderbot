package com.telegrambot.wordreminder.handler.message;

import com.telegrambot.wordreminder.handler.enumeration.HandlerName;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public interface MessageHandler {
    SendMessage handle(Message message);

    HandlerName getHandlerName();
}
