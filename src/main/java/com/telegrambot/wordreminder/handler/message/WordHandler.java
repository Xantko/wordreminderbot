package com.telegrambot.wordreminder.handler.message;

import com.telegrambot.wordreminder.handler.enumeration.CallbackQueryHandlerName;
import com.telegrambot.wordreminder.handler.enumeration.HandlerName;
import com.telegrambot.wordreminder.handler.enumeration.InlineButtonData;
import com.telegrambot.wordreminder.model.dto.PhraseDTO;
import com.telegrambot.wordreminder.model.dto.ProcessedWordDTO;
import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.dto.WordDTO;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import com.telegrambot.wordreminder.model.enumeration.WordStatus;
import com.telegrambot.wordreminder.service.ProcessedWordService;
import com.telegrambot.wordreminder.service.ReplyMessageService;
import com.telegrambot.wordreminder.service.UserService;
import com.telegrambot.wordreminder.service.WordService;
import com.telegrambot.wordreminder.util.CallbackQueryUtil;
import com.telegrambot.wordreminder.util.constant.MessageKeyConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class WordHandler implements MessageHandler {

    private final ReplyMessageService messageService;
    private final UserService userService;
    private final WordService wordService;
    private final ProcessedWordService processedWordService;

    @Override
    public SendMessage handle(Message message) {
        UserDTO user = userService.getUserByUserNumber(message.getFrom().getId().longValue());
        switch (user.getStatus()) {
            case ADD_NEW_WORD_CMD:
                return addNewWordCmdProcess(message, user);
            case ADD_NEW_WORD:
                return addNewWordProcess(message, user);
            case ADD_MEANING:
                return addMeaningProcess(message, user);
            case ADD_PHRASE:
                return addPhrase(message, user);
        }
        return null;
    }

    @Override
    public HandlerName getHandlerName() {
        return HandlerName.WORD;
    }

    private SendMessage addNewWordCmdProcess(Message message, UserDTO user) {
        userService.updateUserStatus(user.getUserNumber(), UserBotStatus.ADD_NEW_WORD);
        return messageService.getReplyMessage(message.getChatId(), MessageKeyConstant.MESSAGE_ADD_NEW_WORD, user.getLangCode().getName());
    }

    private SendMessage addNewWordProcess(Message message, UserDTO user) {
        userService.updateUserStatus(user.getUserNumber(), UserBotStatus.ADD_MEANING);
        WordDTO word = wordService.save(WordDTO.builder()
                .word(message.getText())
                .status(WordStatus.FIRST)
                .build());

        processedWordService.save(ProcessedWordDTO.builder()
                .user(user)
                .word(word)
                .build());
        return messageService.getReplyMessage(message.getChatId(), MessageKeyConstant.MESSAGE_ADD_MEANING, user.getLangCode().getName());
    }

    private SendMessage addMeaningProcess(Message message, UserDTO user) {
        String meaning = message.getText();
        if (StringUtils.isEmpty(meaning)) {
            return messageService.getReplyMessage(message.getChatId(), MessageKeyConstant.MESSAGE_EMPTY_MEANING, user.getLangCode().getName());
        }
        ProcessedWordDTO processedWordDTO = processedWordService.getProcessedWordById(user.getId());
        WordDTO wordDTO = processedWordDTO.getWord();
        wordDTO.setMeaning(meaning);
        wordService.update(wordDTO);
        userService.updateUserStatus(user.getUserNumber(), UserBotStatus.ADD_PHRASE);
        return messageService.getReplyMessage(message.getChatId(), MessageKeyConstant.MESSAGE_ADD_PHRASE, user.getLangCode().getName());
    }

    private SendMessage addPhrase(Message message, UserDTO user) {
        String phraseString = message.getText();
        if (StringUtils.isEmpty(phraseString)) {
            return messageService.getReplyMessage(message.getChatId(), MessageKeyConstant.MESSAGE_EMPTY_PHRASE, user.getLangCode().getName());
        }
        PhraseDTO phrase = PhraseDTO.builder().phrase(phraseString).build();
        ProcessedWordDTO processedWordDTO = processedWordService.getProcessedWordById(user.getId());
        WordDTO wordDTO = processedWordDTO.getWord();
        wordDTO.getPhrases().add(phrase);
        wordService.update(wordDTO);
        userService.updateUserStatus(user.getUserNumber(), UserBotStatus.ASK_TO_ADD_EXTRA_PHRASE);

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(new InlineKeyboardButton().setText(MessageKeyConstant.INLINE_BUTTON_ADD_EXTRA_PHRASE)
                .setCallbackData(CallbackQueryUtil.buildCallbackQueryData(CallbackQueryHandlerName.WORD, InlineButtonData.ADD_PHRASE)));
        List<InlineKeyboardButton> rowInline1 = new ArrayList<>();
        rowInline1.add(new InlineKeyboardButton().setText(MessageKeyConstant.INLINE_BUTTON_STOP_ADDING_PHRASE)
                .setCallbackData(CallbackQueryUtil.buildCallbackQueryData(CallbackQueryHandlerName.WORD, InlineButtonData.STOP_ADDING_PHRASE)));
        rowsInline.add(rowInline);
        rowsInline.add(rowInline1);
        markupInline.setKeyboard(rowsInline);

        return messageService.getReplyMessageInlineKeyboard(message.getChatId(), MessageKeyConstant.MESSAGE_ADD_EXTRA_PHRASE, user.getLangCode().getName(), markupInline);
    }
}
