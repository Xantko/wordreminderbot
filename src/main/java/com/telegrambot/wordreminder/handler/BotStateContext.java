package com.telegrambot.wordreminder.handler;

import com.telegrambot.wordreminder.handler.enumeration.HandlerName;
import com.telegrambot.wordreminder.handler.message.MessageHandler;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BotStateContext {
    private Map<HandlerName, MessageHandler> messageHandlers = new HashMap<>();

    public BotStateContext(List<MessageHandler> messageServices) {
        messageServices.forEach(handler -> this.messageHandlers.put(handler.getHandlerName(), handler));
    }

    public SendMessage processInputMessage(UserBotStatus currentState, Message message) {
        MessageHandler currentMessageHandler = findMessageHandler(currentState);
        return currentMessageHandler.handle(message);
    }

    private MessageHandler findMessageHandler(UserBotStatus currentStatus) {

        if (isWordHandlerState(currentStatus)) {
            return messageHandlers.get(HandlerName.WORD);
        }

        if (isSettingsState(currentStatus)) {
            return messageHandlers.get(HandlerName.SETTINGS);
        }

        if (isHelpState(currentStatus)) {
            return messageHandlers.get(HandlerName.HELP);
        }

        if (UserBotStatus.MAIN_MENU.equals(currentStatus)) {
            return messageHandlers.get(HandlerName.MAIN);
        }

        return messageHandlers.get(HandlerName.EXCEPTION);
    }

    private boolean isWordHandlerState(UserBotStatus currentState) {
        switch (currentState) {
            case ADD_NEW_WORD:
            case ADD_PHRASE:
            case ADD_MEANING:
            case ADD_PHRASE_CMD:
            case ADD_NEW_WORD_CMD:
                return true;
            default:
                return false;
        }
    }

    private boolean isSettingsState(UserBotStatus currentState) {
        switch (currentState) {
            case SETTINGS_MENU:
            case SET_TIMEZONE:
            case VERIFY_TIMEZONE:
            case LANGUAGE_SETTINGS:
            case REMINDER_SETTINGS:
            case SET_WEEKLY_WORDS_NUMBER:
            case VERIFY_TIME:
            case SET_FIRST_STAGE_TIME:
            case SET_SECOND_STAGE_TIME:
            case SET_THIRD_STAGE_TIME:
            case CHOOSE_LANG_NEW_USER:
                return true;
            default:
                return false;
        }
    }

    private boolean isHelpState(UserBotStatus currentState) {
        return currentState == UserBotStatus.HELP;
    }


}
