package com.telegrambot.wordreminder.handler;

import com.telegrambot.wordreminder.handler.callbackquery.CallbackQueryFacade;
import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import com.telegrambot.wordreminder.service.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

@Log4j
@Service
public class TelegramHandlerFacade {

    private UserService userService;
    private BotStateContext botStateContext;
    private CallbackQueryFacade callbackQueryFacade;

    public TelegramHandlerFacade(UserService userService, BotStateContext botStateContext,
                                 CallbackQueryFacade callbackQueryFacade) {
        this.userService = userService;
        this.botStateContext = botStateContext;
        this.callbackQueryFacade = callbackQueryFacade;
    }

    public SendMessage handleUpdate(Update update) {
        SendMessage replyMessage = null;

        if (update.hasCallbackQuery()) {
            return callbackQueryFacade.processCallbackQuery(update.getCallbackQuery());
        }


        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            replyMessage = handleInputMessage(message);
        }

        return replyMessage;
    }

    private SendMessage handleInputMessage(Message message) {
        String inputMsg = message.getText();
        User tgUser = message.getFrom();
        UserDTO userDTO = userService.getUserByUserNumber(tgUser.getId().longValue());
        if (userDTO == null) {
            userDTO = UserDTO.builder()
                    .userNumber(tgUser.getId().longValue())
                    .chatNumber(message.getChat().getId())
                    .name(tgUser.getFirstName())
                    .surname(tgUser.getLastName())
                    .status(UserBotStatus.CHOOSE_LANG_NEW_USER)
                    .build();
            userDTO = userService.saveNewUser(userDTO);
        }
        UserBotStatus botStatus;

        switch (inputMsg) {
            case "/add_word":
                botStatus = UserBotStatus.ADD_NEW_WORD_CMD;
                break;
            case "/word_managing":
                botStatus = UserBotStatus.WORD_MANAGING;
                break;
            case "/settings":
                botStatus = UserBotStatus.SETTINGS_MENU;
                break;
            case "/help":
                botStatus = UserBotStatus.HELP;
                break;
            default:
                botStatus = userDTO.getStatus();
                break;
        }

        userService.updateUserStatus(userDTO.getUserNumber(), botStatus);

        return botStateContext.processInputMessage(botStatus, message);
    }
}
