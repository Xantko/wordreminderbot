package com.telegrambot.wordreminder.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

public interface ReplyMessageService {

    SendMessage getReplyMessage(long chatId, String replyMessageKey, String localeCode);

    SendMessage getReplyMessage(long chatId, String replyMessageKey, String localeCode, Object... args);

    SendMessage getReplyMessageInlineKeyboard(long chatId, String replyMessageKey, String localeCode, InlineKeyboardMarkup inlineKeyboardMarkup, Object... args);
}
