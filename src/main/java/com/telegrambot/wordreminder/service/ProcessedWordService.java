package com.telegrambot.wordreminder.service;

import com.telegrambot.wordreminder.model.dto.ProcessedWordDTO;
import com.telegrambot.wordreminder.model.jpa.ProcessedWord;

public interface ProcessedWordService {

    ProcessedWordDTO save(ProcessedWordDTO processedWord);

    ProcessedWordDTO getProcessedWordById(Long id);

    void delete(ProcessedWordDTO processedWord);
}
