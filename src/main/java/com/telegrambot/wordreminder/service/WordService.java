package com.telegrambot.wordreminder.service;

import com.telegrambot.wordreminder.model.dto.WordDTO;

import java.util.List;

public interface WordService {

    WordDTO getWordById(Long id);

    WordDTO save(WordDTO word);

    void delete(Long id);

    WordDTO update(WordDTO wordDTO);

    List<WordDTO> getWeeklyWordsByUserNumber(Long number);

    List<WordDTO> getAllWordsByUserNumber(Long number);

}
