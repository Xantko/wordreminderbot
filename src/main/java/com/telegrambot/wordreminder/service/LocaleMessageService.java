package com.telegrambot.wordreminder.service;

public interface LocaleMessageService {


    String getMessage(String messageKey, String localeCode);

    String getMessage(String messageKey, String localeCode, Object... args);
}
