package com.telegrambot.wordreminder.service;

import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import org.springframework.transaction.annotation.Transactional;

public interface UserService {

    @Transactional
    UserDTO saveNewUser(UserDTO userDTO);

    UserDTO getUserByUserNumber(Long userNumber);

    @Transactional
    UserDTO updateUserStatus(Long userNumber, UserBotStatus status);

    boolean existsByUserNumber(Long userNumber);
}
