package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.model.dto.ProcessedWordDTO;
import com.telegrambot.wordreminder.model.jpa.ProcessedWord;
import com.telegrambot.wordreminder.model.mapper.ProcessedWordMapper;
import com.telegrambot.wordreminder.repository.ProcessedWordRepository;
import com.telegrambot.wordreminder.service.ProcessedWordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProcessedWordServiceImpl implements ProcessedWordService {

    private final ProcessedWordRepository repository;
    private final ProcessedWordMapper mapper;


    @Override
    public ProcessedWordDTO save(ProcessedWordDTO processedWord) {
        return mapper.toDTO(repository.save(mapper.toEntity(processedWord)));
    }

    @Override
    public ProcessedWordDTO getProcessedWordById(Long id) {
        return mapper.toDTO(repository.findById(id).orElse(null));
    }

    @Override
    public void delete(ProcessedWordDTO processedWord) {
        repository.delete(mapper.toEntity(processedWord));
    }
}
