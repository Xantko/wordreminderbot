package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.service.LocaleMessageService;
import com.telegrambot.wordreminder.service.ReplyMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReplyMessageServiceImpl implements ReplyMessageService {

    private final LocaleMessageService localeMessageService;

    @Override
    public SendMessage getReplyMessage(long chatId, String replyMessageKey, String localeCode) {
        return new SendMessage(chatId, localeMessageService.getMessage(replyMessageKey, localeCode));
    }

    @Override
    public SendMessage getReplyMessage(long chatId, String replyMessageKey, String localeCode, Object... args) {
        return new SendMessage(chatId, localeMessageService.getMessage(replyMessageKey, localeCode, args));
    }

    @Override
    public SendMessage getReplyMessageInlineKeyboard(long chatId, String replyMessageKey, String localeCode, InlineKeyboardMarkup inlineKeyboardMarkup, Object... args) {
        for (List<InlineKeyboardButton> row : inlineKeyboardMarkup.getKeyboard()) {
            for (InlineKeyboardButton button : row) {
                button.setText(localeMessageService.getMessage(button.getText(), localeCode));
            }
        }
        return new SendMessage(chatId, localeMessageService.getMessage(replyMessageKey, localeCode, args))
                .setReplyMarkup(inlineKeyboardMarkup);
    }
}
