package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.exception.UserNotFoundException;
import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.model.enumeration.UserBotStatus;
import com.telegrambot.wordreminder.model.jpa.User;
import com.telegrambot.wordreminder.model.mapper.UserMapper;
import com.telegrambot.wordreminder.repository.UserRepository;
import com.telegrambot.wordreminder.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;


@Log4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final UserMapper mapper;

    @Override
    @Transactional
    public UserDTO saveNewUser(UserDTO userDTO) {
        User user = mapper.toEntity(userDTO);
        user.setStatus(UserBotStatus.CHOOSE_LANG_NEW_USER);
        user.setCreatedDate(LocalDateTime.now());

        return mapper.toDTO(repository.save(user));
    }

    @Override
    @Cacheable(value = "users", key = "#userNumber")
    public UserDTO getUserByUserNumber(Long userNumber) {
        return mapper.toDTO(repository.findByUserNumber(userNumber)
                .orElse(null));
    }

    @Override
    @CachePut(value = "users", key = "#userNumber")
    @Transactional
    public UserDTO updateUserStatus(Long userNumber, UserBotStatus status) {
        User user = repository.findByUserNumber(userNumber)
                .orElseThrow(() -> new UserNotFoundException("User with number " + userNumber + " not found."));
        user.setStatus(status);
        return mapper.toDTO(repository.save(user));
    }

    @Override
    public boolean existsByUserNumber(Long userNumber) {
        return repository.existsByUserNumber(userNumber);
    }


}
