package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.model.dto.WordDTO;
import com.telegrambot.wordreminder.model.mapper.WordMapper;
import com.telegrambot.wordreminder.repository.WordRepository;
import com.telegrambot.wordreminder.service.WordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WordServiceImpl implements WordService {

    private final WordRepository repository;
    private final WordMapper mapper;

    @Override
    public WordDTO getWordById(Long id) {
        return mapper.toDTO(repository.findById(id).orElse(null));
    }

    @Override
    public WordDTO save(WordDTO word) {
        return mapper.toDTO(repository.save(mapper.toEntity(word)));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public WordDTO update(WordDTO wordDTO) {
        if (wordDTO.getId() == null) {
            throw new IllegalArgumentException("Cannot update word with null id");
        }
        return mapper.toDTO(repository.save(mapper.toEntity(wordDTO)));
    }

    @Override
    public List<WordDTO> getWeeklyWordsByUserNumber(Long number) {
        return null;
    }

    @Override
    public List<WordDTO> getAllWordsByUserNumber(Long number) {
        return null;
    }
}
