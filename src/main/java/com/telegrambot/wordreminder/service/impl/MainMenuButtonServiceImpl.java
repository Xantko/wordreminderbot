package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.model.enumeration.LangCode;
import com.telegrambot.wordreminder.service.LocaleMessageService;
import com.telegrambot.wordreminder.service.MainMenuButtonService;
import com.telegrambot.wordreminder.util.constant.MessageKeyConstant;
import com.vdurmont.emoji.EmojiParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MainMenuButtonServiceImpl implements MainMenuButtonService {

    private final LocaleMessageService messageService;

    @Override
    public ReplyKeyboardMarkup getMainMenuButtons(LangCode code) {
        final ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();
        KeyboardRow row3 = new KeyboardRow();
        row1.add(new KeyboardButton(EmojiParser.parseToUnicode(":new: ") + messageService.getMessage(MessageKeyConstant.MENU_BUTTON_ADD_NEW_WORD, code.getName())));
        row2.add(new KeyboardButton(EmojiParser.parseToUnicode(":spiral_note_pad: ") + messageService.getMessage(MessageKeyConstant.MENU_BUTTON_WORD_MANAGING, code.getName())));
        row3.add(new KeyboardButton(EmojiParser.parseToUnicode(":gear: ") + messageService.getMessage(MessageKeyConstant.MENU_BUTTON_SETTINGS, code.getName())));
        row3.add(new KeyboardButton(EmojiParser.parseToUnicode(":question: ") + messageService.getMessage(MessageKeyConstant.MENU_BUTTON_HELP, code.getName())));
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    @Override
    public ReplyKeyboardMarkup getSettingsMenuButtons(LangCode code) {
        return null;
    }
}
