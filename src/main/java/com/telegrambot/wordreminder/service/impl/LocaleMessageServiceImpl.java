package com.telegrambot.wordreminder.service.impl;

import com.telegrambot.wordreminder.service.LocaleMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class LocaleMessageServiceImpl implements LocaleMessageService {

    private final MessageSource messageSource;

    @Override
    public String getMessage(String messageKey, String localeCode) {
        return messageSource.getMessage(messageKey, null, Locale.forLanguageTag(localeCode));
    }

    @Override
    public String getMessage(String messageKey, String localeCode, Object... args) {
        return messageSource.getMessage(messageKey, args, Locale.forLanguageTag(localeCode));
    }


}
