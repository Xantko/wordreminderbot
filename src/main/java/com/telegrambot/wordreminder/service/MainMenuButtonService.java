package com.telegrambot.wordreminder.service;

import com.telegrambot.wordreminder.model.enumeration.LangCode;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;

public interface MainMenuButtonService {

    ReplyKeyboardMarkup getMainMenuButtons(LangCode code);

    ReplyKeyboardMarkup getSettingsMenuButtons(LangCode code);
}
