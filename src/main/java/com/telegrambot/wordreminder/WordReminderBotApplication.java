package com.telegrambot.wordreminder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.telegram.telegrambots.ApiContextInitializer;

@EnableCaching
@SpringBootApplication
public class WordReminderBotApplication {

    public static void main(String[] args) {
        ApiContextInitializer.init();

        SpringApplication.run(WordReminderBotApplication.class, args);
    }
}
