package com.telegrambot.wordreminder.repository;

import com.telegrambot.wordreminder.model.jpa.ProcessedWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessedWordRepository extends JpaRepository<ProcessedWord, Long> {

}
