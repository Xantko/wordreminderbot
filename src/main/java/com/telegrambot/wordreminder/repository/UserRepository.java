package com.telegrambot.wordreminder.repository;

import com.telegrambot.wordreminder.model.jpa.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUserNumber(Long userNumber);

    Optional<User> findByUserNumber(Long userNumber);
}
