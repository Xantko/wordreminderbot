package com.telegrambot.wordreminder.repository;

import com.telegrambot.wordreminder.model.jpa.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {

    Word findByUser_UserNumber(Long number);

    List<Word> findAllByUser_UserNumber(Long number);

}
