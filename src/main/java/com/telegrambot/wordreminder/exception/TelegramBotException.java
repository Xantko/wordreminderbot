package com.telegrambot.wordreminder.exception;

public class TelegramBotException extends RuntimeException {

    public TelegramBotException() {
    }

    public TelegramBotException(String message) {
        super(message);
    }
}
