package com.telegrambot.wordreminder.controller;

import com.telegrambot.wordreminder.model.dto.UserDTO;
import com.telegrambot.wordreminder.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.objects.Update;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @GetMapping("/{id}")
    public UserDTO getUserByNumber(@PathVariable("id") Long id) {
        return service.getUserByUserNumber(id);
    }

    @PostMapping("/")
    public String sendAnswer(@RequestBody Update update) {
        return update.getMessage().getText();
    }
}
